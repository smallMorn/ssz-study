package com.ssz.mul.element;

import com.ssz.mul.constants.MulConstant;

import java.util.Collection;
import java.util.Map;

public class ElementRegisterId implements Element{

    @Override
    public String get(Map<String, Object> map) {
        return gain(map, MulConstant.REGISTER_ID_KEY, MulConstant.DEFAULT_REGISTER_ID);
    }

}
