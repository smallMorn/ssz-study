package com.ssz.mul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SszMulApplication {

    public static void main(String[] args) {
        SpringApplication.run(SszMulApplication.class, args);
    }

}
