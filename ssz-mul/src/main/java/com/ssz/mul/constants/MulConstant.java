package com.ssz.mul.constants;

public interface MulConstant {

    String DISCOVERY_PREFIX = "mul.discovery";

    /**
     * 默认标签字段命名
     */
    String ROUTE_FIELD_NAME = "route";


    /**
     * 是否生效
     */
    String ENABLED = "enabled";

    /**
     * 默认的registerId
     */
    String DEFAULT_REGISTER_ID = "mul_default";

    String LOADBALANCER_PREFIX = "mul.loadbalancer";

    /**
     * 默认标签路由名称
     */
    String DEFAULT_ROUTE = "default";

    /**
     * 拼接符
     */
    String JOINT = "&&";


    /**
     * 默认实例缓存时间 单位/秒
     */
    int DEFAULT_PULL_INTERVAL = 5;

    String HINT = "unrealized";


    String ROUTE_KEY = "mul.route";

    String GROUP_KEY = "mul.group";

    String REGISTER_ID_KEY = "mul.registerId";

}
