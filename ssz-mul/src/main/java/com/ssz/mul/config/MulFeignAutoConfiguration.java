package com.ssz.mul.config;

import com.ssz.mul.constants.MulConstant;
import com.ssz.mul.feign.MulFeignInterceptor;
import com.ssz.mul.ribbon.HeaderThreadLocal;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({RequestInterceptor.class, RequestTemplate.class})
@AutoConfigureBefore({LoadBalancerConfiguration.class})
@ConditionalOnProperty(prefix = MulConstant.DISCOVERY_PREFIX, value = MulConstant.ENABLED, matchIfMissing = true)
public class MulFeignAutoConfiguration {

    @Bean
    public RequestInterceptor mulFeignInterceptor(HeaderThreadLocal local, DiscoveryProperties discoveryProperties) {
        return new MulFeignInterceptor(discoveryProperties, local);
    }

}
