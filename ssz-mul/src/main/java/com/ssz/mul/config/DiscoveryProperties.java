package com.ssz.mul.config;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.ssz.mul.constants.MulConstant;
import com.ssz.mul.nacos.NacosRegister;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(MulConstant.DISCOVERY_PREFIX)
@Data
@Slf4j
public class DiscoveryProperties {

    /**
     * route属性的值
     */
    public String route = "default";

    /**
     * route属性的名称
     */
    public String routeFieldName;

    /**
     * 需要监听的注册中心
     */
    private List<NacosRegister> register = new ArrayList<>();

    /**
     * 拉取实例空次数，阈值达到后自动关闭定时拉取任务
     */
    private Integer pullEmptyCount;

    /**
     * 实例拉取间隔 默认5s
     */
    private Integer pullInterval;

    @Resource
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @PostConstruct
    public void init() {
        this.overrideFromEnv();
    }

    private void overrideFromEnv() {
        if (StringUtils.isEmpty(getRouteFieldName())) {
            setRouteFieldName(MulConstant.ROUTE_FIELD_NAME);
        }
        String nacosRoute = nacosDiscoveryProperties.getMetadata().get(getRouteFieldName());
        if (!StringUtils.isEmpty(nacosRoute)) {
            setRoute(nacosRoute);
        } else {
            nacosDiscoveryProperties.getMetadata().put(getRouteFieldName(), getRoute());
        }
        initializeRegister();
    }

    private void initializeRegister() {
        //缺少元素赋予默认值
        int i = 0;
        for (NacosRegister register : getRegister()) {
            if (StringUtils.isEmpty(register.getNamespace())) {
                register.setNamespace(nacosDiscoveryProperties.getNamespace());
            }
            if (StringUtils.isEmpty(register.getRemoteAddress())) {
                register.setRemoteAddress(nacosDiscoveryProperties.getServerAddr());
            }
            if (StringUtils.isEmpty(register.getRegisterId())) {
                register.setRegisterId(MulConstant.DEFAULT_REGISTER_ID);
                if (++i > 1) {
                    log.warn("存在多个未赋registerId配置项，将以最新未赋registerId的配置项设为默认，默认赋值后属性：{}", register.toString());
                }
            }
            if (nacosDiscoveryProperties.getServerAddr().startsWith("http")) {
                if (StringUtils.isEmpty(register.getUsername())) {
                    register.setUsername(nacosDiscoveryProperties.getUsername());
                }
                if (StringUtils.isEmpty(register.getPassword())) {
                    register.setPassword(nacosDiscoveryProperties.getPassword());
                }
            }
        }
        if (getRegister().stream().noneMatch(register -> MulConstant.DEFAULT_REGISTER_ID.equals(register.getRegisterId()))) {
            NacosRegister register = new NacosRegister();
            register.setRegisterId(MulConstant.DEFAULT_REGISTER_ID);
            register.setNamespace(nacosDiscoveryProperties.getNamespace());
            register.setRemoteAddress(nacosDiscoveryProperties.getServerAddr());
            if (nacosDiscoveryProperties.getServerAddr().startsWith("http")) {
                register.setUsername(nacosDiscoveryProperties.getUsername());
                register.setPassword(nacosDiscoveryProperties.getPassword());
            }
            this.getRegister().add(register);
        }
    }
    /**
     * 服务所属的分组
     */
    public String getGroup() {
        return nacosDiscoveryProperties.getGroup();
    }

}
